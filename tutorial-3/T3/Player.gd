extends KinematicBody2D

# Declare member variables here. Examples:
export (int) var speed = 400
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var key_up_pressed = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play_animation(animation):
    if $AnimationPlayer.current_animation != animation:
       $AnimationPlayer.play(animation)

func get_input():
    velocity.x = 0
          
    if Input.is_action_pressed('right'):
        play_animation("walk_animation")
        velocity.x += speed
    elif Input.is_action_pressed('left'):
        play_animation("walk_animation")
        velocity.x -= speed
    else:
        velocity.x = 0
        if is_on_floor():
            play_animation("idle_animation")
    
    if key_up_pressed < 2 and Input.is_action_just_pressed('up'):
        velocity.y = jump_speed
        play_animation("jump_animation")
        key_up_pressed += 1      

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    velocity.y += delta * GRAVITY
    get_input()
    velocity = move_and_slide(velocity, UP)
    if is_on_floor():
        key_up_pressed = 0
