# Doing the Additional Features

Modifikasi yang kali ini saya tambahkan adalah `LinkButton` untuk kembali ke `MainMenu.tscn`

## Back to Main Menu

1. Pertama, menambahkan node `LinkButton` sebagai child node dari root node `GameOver.tscn` 
2. Pada node `LinkButton` tersebut ditambahkan script untuk menuju ke `MainMenu.tscn`:

```
extends LinkButton

# Declare member variables here
export(String) var scene_to_load

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

func _on_Backto_MainMenu_pressed():
    get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
# Called every frame. 'delta' is the elapsed time since the previous frame.
# func _process(delta):
#    pass
```
3. Kemudian pada inspector mendefinisikan Scene To Load dengan value "MainMenu"

## Referensi

- [CSUI Game Development: tutorial-5](https://gitlab.com/csui-gamedev/tutorials/tree/master/tutorial-5)
