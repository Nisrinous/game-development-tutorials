extends LinkButton

# Declare member variables 
export(String) var scene_to_load

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_New_Game_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_LinkButton_pressed():
	pass # Replace with function body.
